// utlity/db/sqlite3.js

var sqlite3 = require('sqlite3').verbose();
var db = null;
var open = 0;

function initialize() {
  db.run("CREATE TABLE if not exists recipient (id INTEGER PRIMARY KEY AUTOINCREMENT, phoneNumber TEXT, isAdmin INTEGER, sendCredits INTEGER)");
  db.run("CREATE TABLE if not exists notifications (id INTEGER PRIMARY KEY AUTOINCREMENT, message TEXT )");
}

module.exports = {
  getConnection : function() {
    if (db == null) {
      db = new sqlite3.Database('notifire.db');
      initialize();
    }
  },
  close : function() {
    if (users > 0) {
      open = open - 1;
    }

    if (open == 0 && db != null) {
      db.close();
      db = null;
    }
  }
}

process.on('SIGTERM', function(){
  if (db != null) {
    db.close();
  }
})
