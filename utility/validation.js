// validation.js

module.exports = {
  /*
   * Validate that the propertyName parameter exists and is set in the
   * configObject.
   */
  isset : function(configObject, propertyName, success, error) {
    if (!configObject.hasOwnProperty(propertyName)
      || typeof(configObject[propertyName]) == 'undefined'
      || configObject[propertyName] == '') {
        error();
      } else {
        success();
      }
  }
}
