// twilioClient.js

var config = require('../config/config.json' );
var validation = require('../utility/validation.js');
var winston = require('winston')

// Validate that the required elements exist in the configuration file.
validation.isset(config.twilio, 'sid', function(){}, function(){
  winston.error('Check config file, twilio.sid must exist and be set');
  process.exit(1);
});

validation.isset(config.twilio, 'authToken', function(){}, function(){
  winston.error('Check config file, twilio.authToken must exist and be set');
  process.exit(1);
});

validation.isset(config.twilio, 'sendingNumber', function(){}, function(){
  winston.error('Check config file, twilio.sendingNumber must exist and be set');
  process.exit(1);
});

var client = require('twilio')(config.twilio.sid, config.twilio.authToken);

module.exports = {
  /*
   * Send an SMS using the twilio api.
   */
  send : function (recipient, message) {
    client.messages.create ({
      body: message,
      to: recipient,
      from: config.twilio.sendingNumber
    }, function(err, data){
      if (typeof error != 'undefined') {
        //@todo: fix me
        winston.error("Failed sending message." + JSON.stringify(err));
      }
    });
  }
}
