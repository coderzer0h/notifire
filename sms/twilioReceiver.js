// twilioReceiver.js

var restify = require('restify');
var winston = require('winston');
var messageProcessor = require('../messageProcessor.js');

module.exports = {
  /*
   * Listen for incoming web hook calls.
   */
  listen : function() {
    var server = restify.createServer();
    server.name = 'Notifire Twilio Receiver';
    server.use(restify.bodyParser());
    server.use(restify.jsonp());
    server.use(restify.queryParser());

    server.post('/message', messageProcessor.process);

    server.listen(8888, function() {
    winston.info('%s listening at %s', server.name, server.url);
    });
  }
}
