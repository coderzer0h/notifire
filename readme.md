# NotiFire

A super simple SMS notification system using the Twilio backend to send SMS messages. All administration is accomplished using incoming SMS.

## Valid Commands

### Context : superAdmin

- PROMOTE <phone number> - Promote a registered user to an administrator. (UNIMPLEMENTED)
- DEMOTE  <phone number> - Demote a registered user to user status. (UNIMPLEMENTED)

### Context : administrator

### Context : user
