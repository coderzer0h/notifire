// messageProcessor.js

var twilioClient = require('./sms/twilioClient.js');
var config = require('./config/config.json');
var db = require('./utility/db/sqlite3');

var winston = require('winston');

function route(params) {
  var isSuperAdmin = false;
  var isAdmin = false;

  if (params.From == config.superAdminPhone) {
    isSuperAdmin = true;
  }
/*
  var sldb = db.getConnection();
  sldb.get("SELECT from recipient WHERE phoneNumber = ? and isAdmin = 1", params.From, function();
  db.close();
*/

  if (params.Body.match(/^REGISTER/).length) {
    winston.info('Register');
  }

}

/*
 * Return the text SMS to the sending party.
 */
function echo(params) {
  winston.info('Outgoing SMS from %s - message: %s', params.From, params.Body);
  twilioClient.send(params.From, params.Body);
}

module.exports = {
  /*
   * Process the incoming SMS web hook.
   */
  process : function(request, response, next) {
    winston.info('Incoming SMS from %s - message: %s', request.params.From, request.params.Body);
    // console.log(request.param.From);
    response.send();
    route(request.params);
    next();
  }
}
